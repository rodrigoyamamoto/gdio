**All issues and contributions should be done on
[Gitlab](https://gitlab.com/costrouc/python-package-template). Github
is used only as a mirror for visibility**

# GDIO - Gridded Data IO




# Requirements



# Contributing

All contributions, bug reports, bug fixes, documentation improvements,
enhancements and ideas are welcome. These should be submitted at the
[Gitlab repository](https://gitlab.com/costrouc/python-package-template). Github is
only used for visibility.

The goal of this project is to in an opinionated way guide modern
python packaging development for myself.

# License

GNU GPLv3
