import unittest

from gdio.netcdf import netcdf
from gdio.commons import near_yx


class TestNcFiles(unittest.TestCase):


    def setUp(self):

        self.expected_dim = (4, 1, 400, 200)
        self.expected_times = [3, 6, 9, 12]
        self.expected_coordinate = ([65], [134])

        nc = netcdf()
        self.nc = nc.nc_load('data/era5_20191227.nc',
                                   cut_domain=(-30,300,10,320),
                                   cut_time=(3, 12))

    def test_open_netcdf(self):

        self.assertIsNotNone(None if self.nc is {} else 1)

    def test_netcdf_variables_test(self):

        self.assertEqual(list(self.nc.keys()),
                         ['time', 'longitude', 'latitude', 'u10', 'v10', 'd2m', 't2m', 'sp', 'ssrd', 'tp', 'time_unit', 'ref_time'],
                         'incorrect number of variables')

    def test_netcdf_varible_dimension(self):

        self.assertEqual(self.nc.get('tp').shape, self.expected_dim,
                         'dimension shape of u10 variable incorrect')

    def test_grib_cut_time(self):

        self.assertListEqual(list(self.nc.get('time')), self.expected_times,
                         'incorrect time cut')


    def test_grib_cut_space(self):

        self.assertEqual(near_yx({'latitude': self.nc.get('latitude'),
                                  'longitude': self.nc.get('longitude')},
                                lats=-23.54, lons=-46.64), self.expected_coordinate,
                         'problem with the spatial dimension')



if __name__ == '__main__':
    unittest.main()
    pass
