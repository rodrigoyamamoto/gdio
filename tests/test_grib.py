import unittest

from gdio.grib import grib
from gdio.commons import near_yx


class TestGribFiles(unittest.TestCase):


    def setUp(self):

        self.expected_dim = (4, 1, 400, 200)
        self.expected_variables = ['ref_time', 'time_unit', 'time', 'latitude', 'longitude',
                                   'sp', '10u', '10v', '2t', '2d', 'ssrd', 'tp']
        self.expected_times = [3, 6, 9, 12]
        self.expected_coordinate = ([65], [134])

        gr = grib(verbose=False)
        self.gbr = gr.gb_load('data/era5_20191227.grib',
                                   cut_domain=(-30,300,10,320),
                                   cut_time=(3, 12))

    def test_open_grib(self):

        self.assertIsNotNone(None if self.gbr is {} else 1)

    def test_grib_variables_test(self):

        self.assertEqual(list(self.gbr.keys()), self.expected_variables,
                         'incorrect number of variables')

    def test_grib_varible_dimension(self):

        self.assertEqual(self.gbr.get('10u').shape, self.expected_dim,
                         'dimension shape of 10u variable incorrect')

    def test_grib_cut_time(self):

        self.assertListEqual(list(self.gbr.get('time')), self.expected_times,
                         'incorrect time cut')

    def test_grib_cut_space(self):

        self.assertEqual(near_yx({'latitude': self.gbr.get('latitude'),
                                  'longitude': self.gbr.get('longitude')},
                                lats=-23.54, lons=-46.64), self.expected_coordinate,
                         'problem with the spatial dimension')



if __name__ == '__main__':
    unittest.main()
    pass


