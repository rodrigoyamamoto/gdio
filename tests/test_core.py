import unittest

from gdio.core import gdio
from gdio.commons import near_yx


class TestNcFiles(unittest.TestCase):


    def setUp(self):

        self.expected_dim = (4, 1, 400, 200)
        self.expected_times = [3, 6, 9, 12]
        self.expected_coordinate = ([65], [134])

        nc = gdio(verbose=False)
        # self.nc = nc.nc_load('data/era5_20191227.nc',
        #                            cut_domain=(-30,300,10,320),
        #                            cut_time=(3, 12))




if __name__ == '__main__':
    # unittest.main()
    pass



ds = gdio(verbose=False)
ds.mload([
    # 'data/era5_20191227.grib',
    # 'data/era5_20191227.grib',
    'data/gfs.grb'
],
         vars=[
             '10u',
               # '2t'
               ],
         merge_files=True,
         uniformize_grid=True,
         cut_domain=(-30, 300, 10, 320),
         # cut_time=(0, 12),
         inplace=True)


k=0
print(ds.dataset[k].keys())
print(ds.dataset[k]['10u'].shape)
print('time', ds.dataset[k]['time'])
print('ref_time',ds.dataset[k]['ref_time'])
# k=1
# print(gd.dataset[k].keys())
# # print(gd.dataset[k]['tp'].shape)
# print(gd.dataset[k]['time'])
# print(gd.dataset[k]['ref_time'])